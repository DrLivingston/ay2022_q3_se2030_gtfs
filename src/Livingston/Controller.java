package Livingston;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;


import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private HBox letterBox;
    @FXML
    private HBox numberBox;
    //Subject
    private Data data;

    //Observers
    private WordCount wordCountText;
    private NumberCount numberCountText;


    @FXML
    private void load(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(null);
        data.parseFile(selectedFile);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        data = new Data();

        numberCountText = new NumberCount(data);
        wordCountText = new WordCount(data);

        letterBox.getChildren().add(wordCountText);
        numberBox.getChildren().add(numberCountText);

        data.attach(numberCountText);
        data.attach(wordCountText);

    }

}



