package Livingston;

import javafx.scene.control.TextField;

public class WordCount extends TextField implements Observer {

    private final Subject data;

    public WordCount(Data data) {
        super();
        this.data = data;
    }


    @Override
    public void update() {
        this.setText(Integer.toString(((Data) data).getTotalWords()));
    }
}
