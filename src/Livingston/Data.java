package Livingston;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Data implements Subject {

    //Observers
    private ArrayList<Observer> observers;

    //Local data
    private ArrayList<String> words;
    private ArrayList<Integer> numbers;
    
    public Data(){
        observers = new ArrayList<Observer>();
        words = new ArrayList<String>();
        numbers = new ArrayList<Integer>();
    }
    
    @Override
    public void attach(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer:observers) {
            observer.update();
        }
    }

    public void parseFile(File file) {

        Scanner sc = null;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (sc.hasNextLine()) {
            if (sc.hasNextInt()){
                numbers.add(sc.nextInt());
            } else if (sc.hasNext()){
                words.add(sc.next());
            }
        }
        sc.close();
        notifyObservers();

    }

    public int getTotalNumber(){
        return numbers.size();
    }

    public int getTotalWords() {
        return words.size();
    }
}
