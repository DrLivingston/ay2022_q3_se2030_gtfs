package Livingston;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Driver extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = (Parent) new FXMLLoader(getClass().getResource("front.fxml")).load();
        stage.setTitle("Livingston GTFS");
        stage.setScene(new Scene(root, 300, 250));
        stage.show();




    }
}
