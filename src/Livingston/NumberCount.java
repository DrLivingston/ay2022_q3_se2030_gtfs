package Livingston;


import javafx.scene.control.TextField;

public class NumberCount extends TextField implements Observer {

    //Subject
    private Subject data;

    public NumberCount(Data data){
        super();
        this.data = data;
    }

    @Override
    public void update() {
        this.setText(
                Integer.toString(
                        ((Data)data).getTotalNumber()));
    }
}
